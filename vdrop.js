/*
 * Project: vDrop
 * Description:
 * Author: Vaughan Webber - www.vaughanderful.co.za
 * License:
 * Version: 1.0.0
 * Dependancy: vdrop.css, jquery-1.10.2 (jquery.com)
 * Date: 17/08/2013
 */

/*
 * TODO
 * onchange only fire if selection changed
 * disabled must be var flag
 * dont allow click if disabled
 * setValue public method for setting first selection especially after AJAX update / re-initialise?
 * create hidden input for storing selection for POST
 * multi-line select...
 * public methods through public prototype rethink... access object methods?
 * closeall? click body and close individually incase oneOpen var set to false
 */

;
(function($, window) {
	var pluginName = "vDrop", defaults = {
		onChange: '',
		theme: 'default',
		oneOpen: true
	};

	var dropdowns = [];

	function vDrop(element, options) {
		this.options = $.extend({}, defaults, options);
		this.element = $(element);
		this.selectedItemValue = '';
		this.disabled = false;
		this.expannded = false;
		this.arrow = {
			up: '&#9650;',
			down: '&#9660;'
		};

		this.__constructor();
	}

	vDrop.prototype = {
		__constructor: function() {
			this.init();
			this.events();
		},
		init: function() {
			var scope = this;

			dropdowns.push(this.element);

			this.element
					  .addClass('vdrop')
					  .addClass(this.options.theme)
					  .prepend('<div class="selector"></div>')
					  .append('<div class="arrow">' + this.arrow.down + '</div>')
					  .append('<ul>');

			this.element.find('select').hide();

			$.each(this.element.find('option'), function(i, option) {
				var selected = i == 0 ? ' class="selected"' : '';

				scope.element.find('ul').append('<li><a' + selected + ' href="' + $(option).val() + '">' + $(option).text() + '</a></li>');
			});

			this.element.find('div.selector').text(this.element.find('a.selected').text());
		},
		events: function() {
			var scope = this;

			//bind event to selector
			$(this.element.find('div.selector')).on('click', function() {
				if (!scope.disabled) {
					if (scope.expanded) {
						scope.expanded = false;
						scope.element.find('ul').stop(true, true).fadeOut();
					} else {
						scope.expanded = true;
						scope.element.find('ul').stop(true, true).fadeIn();
					}
				}
			});

			this.element.find('a').on('click', function(event) {
				event.stopPropagation();
			});

			this.element.find('*').on('click', function() {
				$(this).attr('unselectable', 'on').css('user-select', 'none').on('selectstart', false);
			});
		}
	};

	$.fn.vDrop = function(options) {
		return this.each(function() {
			if (!$.data(this, "plugin_" + pluginName)) {
				var vdrop = new vDrop(this, options);

				//TODO check if I can access this
				$.data(this, "plugin_" + pluginName, vdrop);

				//store object in data function for accessing methods publically
				$(this).data('self', vdrop);
			}
		});
	};

	$('html').click(function() {
		window.vDrop.closeAll();
	});

	vDrop.global = function() {
	};

	vDrop.global.prototype = {
		closeAll: function(exclude) {
			$.each(dropdowns, function(i, dropdown) {
				if (exclude != 'undefined' && exclude != $(dropdown.context).attr('id'))
					dropdown.removeClass('expanded').find('.arrow').html('&#9660;');
			});
		}
	};

	window.vDrop = new vDrop.global();
})(jQuery, window);